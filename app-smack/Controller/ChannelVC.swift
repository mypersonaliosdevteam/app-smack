//
//  ChannelVC.swift
//  app-smack
//
//  Created by pogs on 03/08/17.
//  Copyright © 2017 Piero Genovese. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
    }

}
